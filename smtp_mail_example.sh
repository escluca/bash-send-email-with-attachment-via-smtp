#!/bin/bash
####################################################################################
# Environment 
# Set a valit PATH if your script runs via cron 
#PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# FROM should be a single address
# 	MAIL_FROM="<user@samplesite.com>"
MAIL_FROM="<someuser@somedomain.com>" ; # <<< SET this to a valid address

# TO,CC,BCC should be arrays
#	MAIL_TO=("<oneuser@somesite.com>"  "<anotheruser@anothersite.com>")
# In case there is no need for CC or BCC, set the corresponding array as empty:
#	MAIL_BCC=()
MAIL_TO=("<someone@someotherdomain.com>") ; # <<< SET this to a valid address
MAIL_CC=()
MAIL_BCC=()

TMP_MAILFILE="/tmp/smtp_mail_send.mail.$$"
ERR_FILE="/tmp/smtp_mail_send.err"
LOG_FILE="/tmp/smtp_mail_send.log"
####################################################################################
echo '' > ${ERR_FILE}
echo '' > ${LOG_FILE}
# Source smtp_mail_send functions
. ./smtp_mail_send
# Load SMTP server configuration and SET errorlog and log filenames for smtp_mail functions
smtp_mail_loadcfg "./smtp_mail_send.cfg" "${ERR_FILE}"  "${LOG_FILE}"

mail_sender_id() {
	echo -e "Sent from:" 
	echo -e "\tApplication:\tTest bash smtp_mail_send() script" 
	echo -e "\tHost/VPS (FQDN):\t`hostname -f`" 
	echo -e "\tScript name:\t${0}" 
	echo -e "\n" 
}


SUBJECT="Mail Subject"
TEXT_HEADER="$(mail_sender_id)\n\n"
TEXT_FILE="./mail_content.txt"
FILE_ARRAY=("./Gnu_licence.txt.zip" "./ESC-logo-black-red.svg")
TYPE_ARRAY=("application/zip" "image/svg+xml")

# smtp_mail_setheader() 
# Usage: 
#	smtp_mail_setheader FROM  SUBJECT  TO_ARRAY  [CC_ARRAY  [BCC_ARRAY]]
smtp_mail_setheader "${MAIL_FROM}"  "${SUBJECT}" MAIL_TO MAIL_CC  MAIL_BCC

# smtp_mail_send()
# Usage: 
#	smtp_mail_send OUT_FILE TEXT_HEADER [TEXT_FILE [FILENAME_ARRAY  TYPE_ARRAY]]
smtp_mail_send "${TMP_MAILFILE}" "${TEXT_HEADER}" "${TEXT_FILE}" FILE_ARRAY  TYPE_ARRAY
if [ "$?" != "0" ] ; then  
	echo -e "Error while sending email !!!" | tee -a  ${ERR_FILE}
	exit 1 ; 
fi





