# Bash functions to send plain text emails with optional files attachments via an SMTP Server (uses curl)

## smtp_mail_loadcfg()
USAGE:
- CFG_FILENAME: smtp_mail_send.cfg
- ERROR_FILENAME: File to use for Error output
- LOG_FILENAME: File to use for Log output


## smtp_mail_setheader()
USAGE:
	smtp_mail_setheader FROM SUBJECT TO_ARRAY [CC_ARRAY [BCC_ARRAY]]
Parameters:
- FROM:	Sender address (String)
- SUBJECT: Subject (String)
- TO_ARRAY: Array with "To" email addressed
- CC_ARRAY: Array with "Cc" email addressed
- BCC_ARRAY: Array with "Bcc" email addressed

## smtp_mail_send ()
USAGE:
	smtp_mail_send OUT_FILE TEXT_HEADER [TEXT_FILE [FILENAME_ARRAY  TYPE_ARRAY]]
Parameters:
- OUT_FILE: Text file where output is written (email text to send via curl->SMTP)
- TEXT_HEADER: String written in the email before TEXT_FILE
- TEXT_FILE: Content of TEXT_FILE is loaded into email text part
- FILENAME_ARRAY: Array with attachment file names
- TYPE_ARRAY Array with attachments types corresponding to files listed in FILENAME_ARRAY


## Sample usage
see: smtp_mail_example.sh




